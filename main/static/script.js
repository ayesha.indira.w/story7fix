$( function() {
  $( "#accordion" )
    .accordion({
      header: "> div > #head",
      collapsible: true,
      icons:false,
      heightStyle: "content",
      active: 0
    })
  } 
);

$( document ).ready(function() {
  setButtons();
  $(document).on('click', '.down', function(e) {
  var cCard = $(this).closest('.wrapper');
  var tCard = cCard.next('.wrapper');
  cCard.insertAfter(tCard);
  setButtons();
  });
  
  $(document).on('click', '.up', function(e) {
  var cCard = $(this).closest('.wrapper');
  var tCard = cCard.prev('.wrapper');
  cCard.insertBefore(tCard);
  setButtons();
  });
    
    function setButtons(){
        $('button').show();
        $('.wrapper:last-child  button.down').hide();
        $('.wrapper:first-child  button.up').hide();
      }
    });