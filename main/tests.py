from django.test import TestCase, Client
from .views import index

# Create your tests here.
class TestStory7(TestCase):
    def test_apakah_url_story7_ada(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
    
    def test_apakah_story7_ada_templatenya(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_apakah_ada_text_story_7_di_halamannya(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 7", html_kembalian)